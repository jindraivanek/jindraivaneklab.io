<h1>Jindřich Ivánek: personal pages</h1>

F# developer and mathematician at MSPS s.r.o.

Prague, Czech Republic

* [Projects](projects)
* [Snippets](snippets)

Github: @jindraivanek

Gitlab: @jindraivanek

Twitter: @jindraivanek